package ru.unn.lesson3;

public class Circle extends Figure {
    private double S;
    private double R;

    public Circle(int x, int y) {
        super(x, y);
    }
    @Override
    public String figureName() {
        return "Круг - ";
    }

    public void getQuadrant(int x, int y) {
        super.getQuadrant(x, y);
    }

    @Override
    public void square() {
        int first = 1;
        int last = 10;
        double R = first + (int) (Math.random()* last);
        S = Math.pow(R,2) * Math.PI;
        System.out.println("Площадь круга: " + S);

    }

    public double getS() {
        return S;
    }

    public void setS() {
        this.S = S;
    }

    public double getR() {
        return R;
    }

    public void setR() {
        this.R = R;
    }

}