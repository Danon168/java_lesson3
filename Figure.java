package ru.unn.lesson3;

public abstract class Figure {
    private String fName;
    private int x;
    private int y;
    private int fourth;


    public Figure(int x, int y) {
        x = x;
        y = y;
    }

    public abstract  String figureName();

    public abstract void square();

    public void getQuadrant(int x , int y) {
        if (x > 0 && y > 0) {
            fourth = 1;
        } else if (x < 0 && y > 0) {
            fourth = 2;
        } else if (x < 0 && y < 0) {
            fourth = 3;
        } else if (x > 0 && y < 0) {
            fourth = 4;
        }
    }

    public int getX() {
        return x;
    }

    public void setX() {
        this.x = x;
    }

    public int getQuarter() {
        return fourth;
    }

    public int getY() {
        return y;
    }

    public void setY() {
        this.y = y;
    }
    public String getName() {
        return fName;
    }
    public void setName() {
        this.fName = fName;
    }
}