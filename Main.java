package ru.unn.lesson3;

public class Main {

    static Figure[] figures = new Figure[5];

    public static void main(String[] args) {
        figures[0] = new Circle(3, 6);
        figures[1] = new Circle(1, 5);
        figures[2] = new Rectangle(7, 2);
        figures[3] = new Rectangle(3, 4);
        figures[4] = new Rectangle(3, 2);
        for (int i = 0; i < 5; i++) {
            System.out.print(figures[i].figureName());
            figures[i].square();
        }

    }
}
